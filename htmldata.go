package main

import _ "embed"

//go:embed html/pentagame.css
var htmlCSS string

//go:embed html/pentagame.html
var htmlHTML string

//go:embed html/pentagame.js
var htmlJS string
