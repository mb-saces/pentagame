package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/pkg/browser"
)

func waitForServer(URL string, timeout time.Duration) error {
	ch := make(chan bool)
	go func() {
		for {
			_, err := http.Get(URL)
			if err == nil {
				ch <- true
			}
			time.Sleep(100 * time.Millisecond)
		}
	}()

	select {
	case <-ch:
		return nil
	case <-time.After(timeout):
		return fmt.Errorf("server did not reply after %v", timeout)
	}
}

func main() {

	var stopChan chan os.Signal

	// TODO add command line parsing

	// TODO add i18n

	mux := http.NewServeMux()

	mux.HandleFunc("/shutdown", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("BYE"))
		stopChan <- os.Interrupt
	})

	mux.HandleFunc("/health", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNoContent)
	})

	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		switch r.URL.Path {
		case "/", "/index.html", "/pentagame.html":
			w.Header().Set("content-Type", "text/html")
			w.WriteHeader(http.StatusOK)
			w.Write([]byte(htmlHTML))
		case "/pentagame.css":
			w.Header().Set("content-Type", "text/css")
			w.WriteHeader(http.StatusOK)
			w.Write([]byte(htmlCSS))
		case "/pentagame.js":
			w.Header().Set("content-Type", "application/javascript")
			w.WriteHeader(http.StatusOK)
			w.Write([]byte(htmlJS))
		default:
			w.WriteHeader(http.StatusNotFound)
		}
	})

	server := &http.Server{
		Addr:    ":8080",
		Handler: mux,
	}

	go func() {
		time.Sleep(3 * time.Second)
		if err := server.ListenAndServe(); !errors.Is(err, http.ErrServerClosed) {
			log.Fatalf("HTTP server error: %v", err)
		}
		log.Println("Stopped serving new connections.")
	}()

	// wait for the server to be ready
	err := waitForServer("http://localhost:8080/health", 10*time.Second)
	if err != nil {
		fmt.Fprintf(os.Stderr, "FATAL: %v", err)
		os.Exit(1)
	}

	// open browser
	browser.OpenURL("http://localhost:8080")

	stopChan = make(chan os.Signal, 1)
	signal.Notify(stopChan, syscall.SIGINT, syscall.SIGTERM)
	<-stopChan

	shutdownCtx, shutdownRelease := context.WithTimeout(context.Background(), 10*time.Second)
	defer shutdownRelease()

	if err := server.Shutdown(shutdownCtx); err != nil {
		log.Fatalf("HTTP shutdown error: %v", err)
	}
	log.Println("Graceful shutdown complete.")
}
